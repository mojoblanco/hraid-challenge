<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Jobs\SendBillToUsers;

class HomeController extends Controller
{
    public function index()
    {
        $users = User::all();
        foreach ($users as $user) {
            SendBillToUsers::dispatch($user);
        }

        return "Process started...";
    }


}
